#+TITLE: Karika QGIS 3 Processing Plugin Help
#+AUTHOR: Roman Geisthövel
#+OPTIONS: toc:nil html-postamble:nil

 # https://plugins.qgis.org/plugins/karika/

* Introduction
  
This processing module implements raster generalization by line integral convolution of the raster values along the surface gradient, as described in the PhD thesis [[https://doi.org/10.3929/ethz-b-000201368][Automatic Swiss style rock depiction]].

In the images below, hillshading is used to demonstrate the effect of this method: the original elevation raster is on the *left*, the generalized raster is on the *right*.

#+NAME: fig.demo
[[./demo.jpg]]

Cells within a distance of D=⌊integration length/2⌋+1 are regarded as invalid, since the integration may be incomplete due to the missing data beyond the raster boundaries. 

Add a buffer margin of size D around your region of interest to ensure the validity all generalized cell values within that region.

The *Border mode* parameter (see below) lets you choose a way of dealing with the border values. 

* Parameters
+ Input raster :: Elevation raster.
+ Integration length :: The main parameter, higher values correspond to increased abstraction of the terrain.
+ Band number :: Number of the raster band, defaulting to 1.
+ Z factor :: Elevation value scale, defaulting to 1. Must be greater than zero.
+ Border mode :: Handling of invalid cells up to a distance of D=⌊integration length/2⌋+1 from the raster boundaries:
    - *Keep*: retain the incomplete values; this is the default
    - *Crop*: remove D columns or rows from each side of the raster; the raster extent and geo transform are modified
    - *Fill*: fill the border with the nodata value of the raster (-32768 if raster has no predefined nodata value)
+ Output raster :: Destination raster. Currently, only the GeoTiff file format is supported.

#+NAME: img.gui
[[./gui.png]]
