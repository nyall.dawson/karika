VERSION=$(shell awk 'BEGIN { FS = "=" }; $$1 == "version" { print $$2 }' metadata.txt)

clean:
	find . -type d -name "__pycache__" -exec rm -rf {} \;

zip:
	
	git archive --format zip --prefix "karika/" -o karika_$(VERSION).zip -v -9 HEAD
